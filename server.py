#!/usr/bin/env python3 #
# -*- coding: utf-8 -*-

import os
import sys
import socketserver
import json
import struct
import bencode
import io

import lib

SHOW_HIDDEN = False
CHUNKCACHE_FILE = "chunkcache.bencode"
SOURCE_DIR = "D:/Downloads/Books"

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    """
    path = None
    chunkcache = None
    def handle(self):
        self.path = self.server.path
        if self.chunkcache is None:
            self.__class__.chunkcache = {}
            try:
                if os.path.exists(CHUNKCACHE_FILE):
                    print("Loading chunkcache file")
                    with open(CHUNKCACHE_FILE, "rb") as ch:
                        chunkdata = bencode.load(ch)
                        for k,v in chunkdata.items():
                            self.chunkcache[k] = lib.ChunkedFile().from_dict(v)
            except Exception as e:
                print("Failed to load chunkcache file")

        with self.request.makefile('rwb') as fh:
            print(self.client_address)
            while True:
                res = {}
                try:
                    data = bencode.load(fh, ignore_toomuchdata=True)
                    print(data)
                    if data.get('q').decode('utf-8') == "info":
                        path = lib.parsePath(data.get('path', b'/').decode('utf-8'))
                        realpath = os.path.realpath(os.path.join(self.path, path))

                        print(realpath)
                        if os.path.isdir(realpath):
                            res = {
                                'r': 'info',
                                'type': 'd',
                                'path': path,
                                'list': [],
                            }
                            for li in sorted(os.listdir(realpath)):
                                if not SHOW_HIDDEN and li.startswith('.'):
                                    continue
                                rli = os.path.realpath(os.path.join(realpath, li))
                                if os.path.isdir(rli) or os.path.isfile(rli):
                                    res['list'].append('/'.join([path, li]))
                        elif os.path.isfile(realpath):
                            cached = self.chunkcache.get(path)
                            if cached is None:
                                cached = lib.ChunkedFile(realpath)
                                cached.calculate()
                                self.chunkcache[path] = cached
                            res = {
                                'r': 'info',
                                'type': 'f',
                                'path': path,
                                'info': cached.to_dict(),
                            }
                            res['info']['name'] = path
                    elif data.get('q').decode('utf-8') == "chunk":
                        path = lib.parsePath(data.get('path', b'/').decode('utf-8'))
                        realpath = os.path.realpath(os.path.join(self.path, path))

                        if os.path.isfile(realpath):
                            cached = self.chunkcache.get(path)
                            if cached is None:
                                cached = lib.ChunkedFile(realpath)
                                cached.calculate()
                                self.chunkcache[path] = cached
                            chunk = data.get('chunk')
                            if chunk is not None:
                                res = {
                                    'r': 'chunk',
                                    'path': path,
                                    'chunk': chunk,
                                    'data': cached.get_chunk(chunk),
                                }

                    if res:
                        self.request.sendall(bencode.dumps(res))
                    else:
                        self.request.sendall(bencode.dumps({'r':'error'}))


                except Exception as e:
                    bencode.dump({'r':'error'}, fh)
                    print(e)
                    break

        chunkdata = {}
        for k,v in self.chunkcache.items():
            if v:
                chunkdata[k] = v.to_dict()

        with open(CHUNKCACHE_FILE, "wb") as cf:
            bencode.dump(chunkdata, cf)


if __name__ == "__main__":
    HOST, PORT = "0", 9999

    # Create the server, binding to localhost on port 9999
    try:
        server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)
        server.path = SOURCE_DIR
        server.serve_forever()
    finally:
        try:
            server.server_close()
        except: pass
