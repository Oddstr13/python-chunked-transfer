#!/usr/bin/env python3 #
# -*- coding: utf-8 -*-

import socket
import struct
import json
import sys
import os
import queue

import bencode

import lib

OUTPUT_DIR = "out"
FASTRESUME_FILE = "fastresume.bencode"

class Downloader(object):
    outdir = None
    fastresume = None

    def __init__(self, outdir, server):
        self.outdir = outdir
        self.server = server

    def run(self):
        if self.fastresume is None:
            self.__class__.fastresume = {}
            try:
                if os.path.exists(FASTRESUME_FILE):
                    print("Loading fastresume file")
                    with open(FASTRESUME_FILE, "rb") as ch:
                        resumedata = bencode.load(ch)
                        self.fastresume.update(resumedata)
                        # Make sure file names are unicode strings, not bytestrings
                        self.fastresume['finished'] = [item.decode('utf-8') if type(item) is bytes else item for item in self.fastresume.get('finished', [])]
                else:
                    self.fastresume['finished'] = []
            except:
                print("Failed to load fastresume file")

        try:
            with socket.socket() as self.s:
                self.s.connect(self.server)
                self.s.settimeout(300)
                with self.s.makefile('rwb') as self.fh:
                    pathq = queue.Queue()
                    pathq.put('/')
                    while not pathq.empty():
                        path = pathq.get_nowait()
                        info = self.get_info(path)
                        if info:
                            if info.get('type').decode('utf-8') == 'd':
                                for item in info.get('list'):
                                    pathq.put(item.decode('utf-8'))
                            if info.get('type').decode('utf-8') == 'f':
                                if path in self.fastresume['finished']:
                                    print("{} already completed, skipping..".format(path))
                                    continue

                                cf = lib.ChunkedFile()
                                cf.from_dict(info.get('info'))
                                cf.path = os.path.join(self.outdir, lib.parsePath(path))
                                os.makedirs(os.path.dirname(cf.path), exist_ok=True)

                                for chunk, chunkhash in enumerate(cf.chunks):
                                    if cf.check(chunk):
                                        continue
                                    chunkdata = self.get_chunk(path, chunk)

                                    if chunkdata:
                                        cf.set_chunk(chunk, chunkdata.get('data'))
                                        print("{}:{}: {}".format(path, chunk, 'OK' if cf.check(chunk) else "ERR"))

                                if cf.check():
                                    self.fastresume['finished'].append(path)
        finally:
            with open(FASTRESUME_FILE, "wb") as cf:
                bencode.dump(self.fastresume, cf)




    def query(self, q):
        self.s.sendall(bencode.dumps(q))
        return bencode.load(self.fh, ignore_toomuchdata=True)

    def get_info(self, path='/'):
        res = self.query({
            'q': 'info',
            'path': path,
        })
        if res.get('r').decode('utf-8') == 'info':
            return res

    def get_chunk(self, path, chunk):
        res = self.query({
            'q': 'chunk',
            'path': path,
            'chunk': chunk,
        })
        if res.get('r').decode('utf-8') == 'chunk' and res.get('chunk') == chunk:
            return res

if __name__ == "__main__":
    outpath = OUTPUT_DIR
    if len(sys.argv) > 2:
        outpath = sys.argv[2]
    host, port = sys.argv[1].split(':')
    port = int(port)
    dl = Downloader(outpath, (host, port))
    dl.run()
