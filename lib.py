#!/usr/bin/env python3 #
# -*- coding: utf-8 -*-

import os
import json
import hashlib
import bencode

CMD_CLOSE = 0
CMD_GETINFO = 1
CMD_GETCHUNK = 2

def parsePath(s):
    res = []
    for el in s.replace(os.path.sep, '/').replace(':', '').split('/'):
        if el not in ('', '.'):
            if el == '..':
                if res:
                    res.pop()
            else:
                res.append(el)
    return '/'.join(res)


class ChunkedFile(object):
    path = None
    chunks = None
    size = None
    chunksize = 1*1024*1024

    def __init__(self, path=None, chunksize=1*1024*1024, algo='sha1'):
        assert chunksize % 1024 == 0, "chunksize must be multiple of 1024"
        assert algo in hashlib.algorithms_available, "unknown hashing algorithm `{}`".format(algo)
        self.path = path
        self.chunksize = chunksize
        self.algo = algo

    def calculate(self):
        self.size = os.stat(self.path).st_size
        self.chunks = []
        with open(self.path, "rb") as fh:
            for i in range(self.size // self.chunksize):
                h = hashlib.new(self.algo)
                for j in range(self.chunksize // 1024):
                    h.update(fh.read(1024))
                self.chunks.append(h.digest())

            h = hashlib.new(self.algo)
            last = fh.read()
            h.update(last)
            h.update(b'\x00' * (self.chunksize - len(last)))
            self.chunks.append(h.digest())

    def check(self, chunk=None):
        if not os.path.exists(self.path):
            return False

        with open(self.path, "rb") as fh:
            if chunk is None:
                for i in range(self.size // self.chunksize):
                    h = hashlib.new(self.algo)
                    for j in range(self.chunksize // 1024):
                        h.update(fh.read(1024))
                    if not h.digest() == self.chunks[i]:
                        return False

                h = hashlib.new(self.algo)
                last = fh.read()
                h.update(last)
                h.update(b'\x00' * (self.chunksize - len(last)))
                return h.digest() == self.chunks[-1]

            else:
                fh.seek(self.chunksize * chunk)
                h = hashlib.new(self.algo)
                n = 0
                for j in range(self.chunksize // 1024):
                    data = fh.read(1024)
                    n += len(data)
                    h.update(data)
                h.update(b'\x00' * (self.chunksize - n))
                return h.digest() == self.chunks[chunk]

    def get_chunk(self, chunk):
        with open(self.path, "rb") as fh:
            fh.seek(self.chunksize * chunk)
            return fh.read(self.chunksize)

    def set_chunk(self, chunk, data):
        if not os.path.exists(self.path):
            print("Creating file `{}`...".format(self.path))
            with open(self.path, "wb"): pass

        with open(self.path, "r+b") as fh:
            fh.truncate(self.size)
            fh.seek(self.chunksize * chunk)
            fh.write(data)

    def to_dict(self):
        return {
            'chunksize' : self.chunksize,
            'name'      : self.path,
            'size'      : self.size,
            'chunks'    : self.chunks,
            'algo'      : self.algo,
        }

    def from_dict(self, d):
        self.chunksize = d.get('chunksize')
        self.chunks    = d.get('chunks')
        self.size      = d.get('size')
        self.path      = d.get('name')
        self.algo      = d.get('algo')

        if type(self.algo) is bytes:
            self.algo = self.algo.decode('utf-8')
        if type(self.path) is bytes:
            self.path = self.path.decode('utf-8')

    def to_bytes(self):
        return bencode.bencode(self.to_dict())

    def from_bytes(self, b):
        return self.from_dict(bencode.bdecode(b))

    def copy(self):
        new = self.__class__(self.path)
        new.from_dict(self.to_dict())
        return new



def main():
    cf = ChunkedFile("Outrunning Karma-60izCGahWXo.mkv")
    cf.calculate()
    print(cf.to_dict())
    print(cf.check())
    print(cf.check(0))
    print(cf.check(1))
    ncf = cf.copy()
    ncf.path += ".out"

    for chunk, chunkhash in enumerate(cf.chunks):
        ncf.set_chunk(chunk, cf.get_chunk(chunk))
        print("{}: {}".format(chunk, 'OK' if ncf.check(chunk) else "ERR"))

    print(ncf.check())




if __name__ == "__main__":
    main()
