#!/usr/bin/env python #
# -*- coding: utf-8 -*-
#
#  bencode.py
#  OpenShell Python bencode library
#  Version: 0.04
#
#  Copyright (c) 2018 Odd Stråbø <oddstr13@openshell.no>
#
#  License: MIT
#

import sys

if sys.version_info.major == 2:
    bytes = str
else:
    unicode = str
    long = int

__all__ = [
    'bencode',
    'bdecode',
    'loads',
    'dumps',
    'load',
    'dump',
    'encode',
    'decode',
]

def encode_string(obj):
    if type(obj) is unicode:
        obj = obj.encode("utf-8")
    return unicode(len(obj)).encode('utf-8') + b':' + obj

def encode_int(obj):
    return b'i' + unicode(obj).encode('utf-8') + b'e'

def encode_list(obj):
    res = b'l'
    for i in obj:
        res += detect_encode_type(i)(i)
    res += b'e'
    return res

def encode_dict(obj):
    res = b'd'
    for k in obj:
        v = obj[k]
        res += detect_encode_type(k)(k)
        res += detect_encode_type(v)(v)
    res += b'e'
    return res

def detect_encode_type(obj):
    t = type(obj)

    if t in [bytes, unicode]:
        return encode_string
    if t in [int, long]:
        return encode_int
    if t in [list, tuple]:
        return encode_list
    if t in [dict]:
        return encode_dict

    raise TypeError("Unknown type '%s'" %(t))

def bencode(obj):
    return detect_encode_type(obj)(obj)


# ----- bdecode -----
class DecodeException(Exception): pass
class DictKeyNotString(DecodeException): pass
class TooMuchData(DecodeException): pass

class StringFile(object):
    """
    Provides filehandle-like access to a string.
    """
    def __init__(self, s):
        if type(s) is unicode:
            s = s.encode("utf-8")
        self.s = s
        self.length = len(s)
        self.pos = 0

    def read(self, n):
        if self.pos + n > self.length:
            n = self.length - self.pos
        s = self.s[self.pos:self.pos+n]
        self.pos = self.pos + n
        return s

    def peek(self, n):
        if self.pos + n > self.length:
            n = self.length - self.pos
        s = self.s[self.pos:self.pos+n]
        return s

    def tell(self):
        return self.pos

    def seek(self, offset, whence=0):
        if whence == 0: # SEEK_SET
            self.pos = min(offset, self.length)
        elif whence == 1: # SEEK_CUR
            self.pos += offset
        elif whence == 2: # SEEK_END
            self.pos = max(0, self.length - offset)
        else:
            raise TypeError("whence must be one of [0, 1, 2]")

    def write(self, *args):
        raise IOError()

    def close(self): pass

def decode_dict(data):
    """
    Decode bencoded dictionary
    """
    c = data.peek(1)[:1]
    if not c == b'd':
        raise ValueError("`{0}` is not the start of a bencoded dictionary.".format(c))
    data.read(1)
    res = {}
    while True:
        t = detect_decode_type(data)
        if t == "end":
            return res
        if not t is decode_string: raise DictKeyNotString
        k = t(data).decode("utf-8")
        v = detect_decode_type(data)(data)
        res[k] = v

def decode_string(data):
    """
    Decode bencoded string
    """
    c = data.read(1)
    if not c in b'0123456789': raise ValueError("`{0}` is not the start of a bencoded string.".format(c))
    sLen = b""
    while c != b':':
        sLen += c
        c = data.read(1)
    iLen = int(sLen)
    return data.read(iLen)

def decode_list(data):
    """
    Decode bencoded list
    """
    c = data.read(1)
    if not c == b'l': raise ValueError("`{0}` is not the start of a bencoded list.".format(c))
    res = []
    t = detect_decode_type(data)
    while t != "end":
        res.append(t(data))
        t = detect_decode_type(data)
    return res

def decode_int(data):
    """
    Decode bencoded integer
    """
    c = data.read(1)
    if not c == b'i': raise ValueError("`{0}` is not the start of a bencoded integer.".format(c))
    c = data.read(1)
    sInt = b""
    while c != b'e':
        sInt += c
        c = data.read(1)
    return int(sInt)

def detect_decode_type(data):
    """
    Detect bencode datatype, and return function to decode, or the string end.
    """
    c = data.peek(1)[:1]
    if not c:
        raise TypeError("No data to decode")
    if c in b'd':
        return decode_dict
    if c in b'i':
        return decode_int
    if c in b'l':
        return decode_list
    if c in b'0123456789':
        return decode_string
    if c in b'e':
        data.read(1)
        return "end"
    raise TypeError("Unknown type '%s'" %(c))

def bdecode(s=None, f=None, ignore_toomuchdata=False):
    """
    Decode bencode into python types
      Supply with one of:
        s   A string containing the bencoded data.
        f   A File handle.
      ignore_toomuchdata  Don't raise exception when decoding is done,
            and there still is data left in file/string.
            False by default (raises exception when True)
    """
    if f is None and s is None: raise ValueError
    if not f is None:
        data = f
    elif not s is None:
        data = StringFile(s)

    res = detect_decode_type(data)(data)
    if not ignore_toomuchdata and data.peek(1):
        raise TooMuchData("Decoding done, but there is still data left.")
    return res

def dump(obj, fp, skipkeys=False, *args, **kwargs):
    if skipkeys: raise NotImplementedError("skipkeys is not yet implemented.")
    fp.write(bencode(obj, *args, **kwargs))

def dumps(obj, skipkeys=False, *args, **kwargs):
    '''Encode `obj` using bencode. Returns bytestring.'''
    if skipkeys: raise NotImplementedError("skipkeys is not yet implemented.")
    return bencode(obj, *args, **kwargs)

def load(fp, *args, **kwargs):
    return bdecode(f=fp, *args, **kwargs)

def loads(s, *args, **kwargs):
    return bdecode(s, *args, **kwargs)

encode = bencode
decode = bdecode

def main():
    print(bencode("Hello World!"))
    print(bencode(123))
    print(bencode(-123))
    print(bencode([1, 2, 3, "asd"]))
    print(bencode([1, (2, 3), "asd"]))
    print(bencode({'a': [1, 2, 3], "foo": "Hello World!"}))
    print(bdecode(s="12:Hello World!"))
    test = load(open("python.torrent", "rb"))
    print(test)
    print(bencode(test))
    print(bdecode(bencode(test)))
    print(bdecode("d5:world12:Hello world!e"))
    return 0

if __name__ == '__main__':
    main()
